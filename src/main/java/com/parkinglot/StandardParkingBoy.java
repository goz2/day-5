package com.parkinglot;

import java.util.*;

public class StandardParkingBoy implements ParkingBoy {

    private ParkingLot parkingLot;

    private List<ParkingLot> parkingLots;

    public StandardParkingBoy() {
        parkingLots = new ArrayList<>();
    }

    public void addParkingLot(ParkingLot... parkingLot) {
        parkingLots.addAll(Arrays.asList(parkingLot));
    }

    public StandardParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst()
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isIn(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedException::new);
    }

    @Override
    public boolean isAllFull() {
        return parkingLots.stream().allMatch(ParkingLot::isFull);
    }
}
