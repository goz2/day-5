package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {
    private final Map<Ticket, Car> map = new HashMap<>();
    private int capacity = 10;

    public ParkingLot() {
    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public boolean isIn(Ticket ticket) {
        return map.containsKey(ticket);
    }
    public boolean isFull() {
        return map.size() >= capacity;
    }

    public Ticket park(Car car) {
        if (isFull()) {
            throw new NoAvailablePositionException();
        }
        Ticket ticket = new Ticket();
        map.putIfAbsent(ticket, car);
        ticket.setParkingLot(this);
        return ticket;
    }

    public int getAvailablePositions() {
        return capacity - map.size();
    }

    public Car fetch(Ticket ticket) {
        if (!isIn(ticket)) {
            throw new UnrecognizedException();
        }
        return map.remove(ticket);
    }

    public double getAvailablePositionRate() {
        return (double) getAvailablePositions() / capacity;
    }
}
