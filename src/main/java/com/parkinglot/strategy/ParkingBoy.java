package com.parkinglot.strategy;

import com.parkinglot.Car;
import com.parkinglot.ParkingLot;
import com.parkinglot.Ticket;
import com.parkinglot.UnrecognizedException;

import java.util.List;
import java.util.Optional;

public class ParkingBoy {
    private List<ParkingLot> parkingLots;
    private ParkingStrategy parkingStrategy;

    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public ParkingBoy(List<ParkingLot> parkingLots, ParkingStrategyFactory parkingStrategyFactory) {
        this.parkingLots = parkingLots;
        this.parkingStrategy = Optional.ofNullable(parkingStrategyFactory.registerParkingStrategy())
                .orElse(new ParkingStrategyFactory("com.parkinglot.strategy.SequentialParkingStrategy").registerParkingStrategy());
    }

    public void setParkingStrategy(ParkingStrategyFactory parkingStrategyFactory) {
        this.parkingStrategy = Optional.ofNullable(parkingStrategyFactory.registerParkingStrategy())
                .orElse(new ParkingStrategyFactory("com.parkinglot.strategy.SequentialParkingStrategy").registerParkingStrategy());
    }

    public boolean isAllFull() {
        return parkingLots.stream().allMatch(ParkingLot::isFull);
    }

    public Ticket park(Car car) {
        ParkingLot selectedParkingLot = parkingStrategy.selectParkingLot(parkingLots);
        return selectedParkingLot.park(car);
    }

    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isIn(ticket))
                .map(parkingLot -> parkingLot.fetch(ticket))
                .findFirst()
                .orElseThrow(UnrecognizedException::new);
    }
}
