package com.parkinglot.strategy;

import com.parkinglot.ParkingLot;

import java.util.List;

public interface ParkingStrategy {
    ParkingLot selectParkingLot(List<ParkingLot> parkingLots);
}
