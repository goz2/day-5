package com.parkinglot.strategy;

import java.lang.reflect.InvocationTargetException;

public class ParkingStrategyFactory {
    private String strategyClassName;

    public ParkingStrategyFactory(String strategyClassName) {
        this.strategyClassName = strategyClassName;
    }

    public ParkingStrategy registerParkingStrategy() {
        try {
            Class<?> strategyClass = Class.forName(strategyClassName);
            return (ParkingStrategy) strategyClass.getDeclaredConstructor().newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | NoSuchMethodException |
                 InvocationTargetException e) {
            throw new IllegalArgumentException("Invalid strategy class: " + strategyClassName);
        }
    }
}
