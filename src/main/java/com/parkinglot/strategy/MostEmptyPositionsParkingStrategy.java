package com.parkinglot.strategy;

import com.parkinglot.NoAvailablePositionException;
import com.parkinglot.ParkingLot;

import java.util.Comparator;
import java.util.List;

public class MostEmptyPositionsParkingStrategy implements ParkingStrategy{
    @Override
    public ParkingLot selectParkingLot(List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparingInt(ParkingLot::getAvailablePositions))
                .orElseThrow(NoAvailablePositionException::new);
    }
}
