package com.parkinglot.strategy;

import com.parkinglot.NoAvailablePositionException;
import com.parkinglot.ParkingLot;

import java.util.List;

public class SequentialParkingStrategy implements ParkingStrategy{
    @Override
    public ParkingLot selectParkingLot(List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst().orElseThrow(NoAvailablePositionException::new);
    }
}
