package com.parkinglot;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class ParkingLotServiceManager {

    private List<ParkingBoy> parkingBoys;
    private List<ParkingLot> parkingLots;

    private ParkingBoy currentParkingBoy;

    public ParkingLotServiceManager() {
        this.parkingBoys = new ArrayList<>();
        this.parkingLots = new ArrayList<>();
    }

    public ParkingBoy getCurrentParkingBoy() {
        return currentParkingBoy;
    }

    public void setCurrentParkingBoy(ParkingBoy currentParkingBoy) {
        this.currentParkingBoy = currentParkingBoy;
    }

    public void addParkingBoy(ParkingBoy... parkingBoy) {
        parkingBoys.addAll(List.of(parkingBoy));
    }

    public void addParkingLot(ParkingLot... parkingLot) {
        parkingLots.addAll(List.of(parkingLot));
    }

    public Ticket parkToOwnParkingLot(Car car) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .map(parkingLot -> {
                    Ticket ticket = parkingLot.park(car);
                    ticket.setParkingLot(parkingLot);
                    return ticket;
                })
                .findFirst()
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetchFromOwnParkingLot(Ticket ticket) {
        if (!parkingLots.contains(ticket.getParkingLot())) {
            throw new UnrecognizedException();
        }
        return ticket.getParkingLot().fetch(ticket);
    }

    private ParkingBoy selectParkingBoy() {
        if (!parkingBoys.isEmpty()) {
            ParkingBoy currentParkingBoy = parkingBoys.stream()
                    .filter(parkingBoy -> !parkingBoy.isAllFull())
                    .findFirst()
                    .orElseThrow(NoParkingBoyAvailableException::new);
            setCurrentParkingBoy(currentParkingBoy);
            return currentParkingBoy;
        }

        throw new NoParkingBoyAvailableException();
    }

    public Ticket park(Car car) {
        return Optional.ofNullable(selectParkingBoy())
                .map(parkingBoy -> parkingBoy.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return Optional.ofNullable(getCurrentParkingBoy())
                .map(parkingBoy -> parkingBoy.fetch(ticket))
                .orElseThrow(UnrecognizedException::new);
    }
}
