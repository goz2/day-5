package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SmartParkingBoy implements ParkingBoy {
    private List<ParkingLot> parkingLots;

    public SmartParkingBoy() {
    }

    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public boolean isAllFull() {
        return parkingLots.stream().allMatch(ParkingLot::isFull);
    }

    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparingInt(ParkingLot::getAvailablePositions))
                .map(parkingLot -> parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isIn(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedException::new);
    }
}
