package com.parkinglot;

import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy implements ParkingBoy {
    private List<ParkingLot> parkingLots;

    public <E> SuperSmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
        return parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparingDouble(ParkingLot::getAvailablePositionRate))
                .map(parkingLot ->  parkingLot.park(car))
                .orElseThrow(NoAvailablePositionException::new);
    }

    public Car fetch(Ticket ticket) {
        return parkingLots.stream()
                .filter(parkingLot -> parkingLot.isIn(ticket))
                .findFirst()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .orElseThrow(UnrecognizedException::new);
    }

    @Override
    public boolean isAllFull() {
        return parkingLots.stream().allMatch(ParkingLot::isFull);
    }
}
