package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SmartParkingBoyTest {
    @Test
    void should_return_ticket_when_park_car_given_smart_parking_boy_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        // when
        Ticket ticket = smartParkingBoy.park(car);

        // then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_smart_parking_boy_a_ticket() {
        // given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();
        Ticket parkTicket = smartParkingBoy.park(car);

        // when
        Car car_ = smartParkingBoy.fetch(parkTicket);

        // then
        Assertions.assertNotNull(car_);
    }

    @Test
    void should_return_two_car_when_fetch_car_by_smart_parking_boy_given_two_ticket() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));

        Car car1 = new Car();
        Car car2 = new Car();
        Ticket parkTicket1 = smartParkingBoy.park(car1);
        Ticket parkTicket2 = smartParkingBoy.park(car2);

        // when
        Car car_1 = smartParkingBoy.fetch(parkTicket1);
        Car car_2 = smartParkingBoy.fetch(parkTicket2);

        // then
        Assertions.assertNotNull(car_1);
        Assertions.assertNotNull(car_2);
    }

    @Test
    void should_return_exception_when_fetch_car_by_smart_parking_boy_given_wrong_ticket() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }

    @Test
    void should_return_exception_when_fetch_car_by_smart_parking_boy_given_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));

        Ticket ticket = smartParkingBoy.park(car);
        smartParkingBoy.fetch(ticket);

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> smartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }

    @Test
    void should_return_exception_when_park_car_by_smart_parking_boy_given_parking_lot_no_position() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));

        for (int i = 0; i < 4; i++) {
            Car car_i = new Car();
            smartParkingBoy.park(car_i);
        }
        Car car = new Car();

        // when then
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class,
                        () -> smartParkingBoy.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());
    }

    @Test
    void should_return_correct_parking_log_when_park_car_by_smart_parking_boy_given_car_with_correct_parking_lot() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        // when
        Ticket ticket = smartParkingBoy.park(car);

        // then
        Assertions.assertEquals(parkingLot2, ticket.getParkingLot());
    }

    @Test
    void should_return_correct_parking_log_when_park_car_by_smart_parking_boy_given_car_with_not_correct_parking_lot() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(5);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        // when
        Ticket ticket = smartParkingBoy.park(car);

        // then
        Assertions.assertNotEquals(parkingLot2, ticket.getParkingLot());
    }
}