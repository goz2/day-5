package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class SuperSmartParkingBoyTest {
    @Test
    void should_return_ticket_when_park_car_given_super_smart_parking_boy_a_car() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();

        // when
        Ticket ticket = superSmartParkingBoy.park(car);

        // then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_super_smart_parking_boy_a_ticket() {
        // given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));
        Car car = new Car();
        Ticket parkTicket = superSmartParkingBoy.park(car);

        // when
        Car car_ = superSmartParkingBoy.fetch(parkTicket);

        // then
        Assertions.assertNotNull(car_);
    }

    @Test
    void should_return_two_car_when_fetch_car_by_super_smart_parking_boy_given_two_ticket() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));

        Car car1 = new Car();
        Car car2 = new Car();
        Ticket parkTicket1 = superSmartParkingBoy.park(car1);
        Ticket parkTicket2 = superSmartParkingBoy.park(car2);

        // when
        Car car_1 = superSmartParkingBoy.fetch(parkTicket1);
        Car car_2 = superSmartParkingBoy.fetch(parkTicket2);

        // then
        Assertions.assertNotNull(car_1);
        Assertions.assertNotNull(car_2);
    }

    @Test
    void should_return_exception_when_fetch_car_by_super_smart_parking_boy_given_wrong_ticket() {
        // given
        Ticket ticket = new Ticket();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }

    @Test
    void should_return_exception_when_fetch_car_by_super_smart_parking_boy_given_used_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));

        Ticket ticket = superSmartParkingBoy.park(car);
        superSmartParkingBoy.fetch(ticket);

        // when then
        UnrecognizedException unrecognizedException = Assertions.assertThrows(UnrecognizedException.class, () -> superSmartParkingBoy.fetch(ticket));
        Assertions.assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }

    @Test
    void should_return_exception_when_park_car_by_super_smart_parking_boy_given_parking_lot_no_position() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2));

        for (int i = 0; i < 4; i++) {
            Car car_i = new Car();
            superSmartParkingBoy.park(car_i);
        }
        Car car = new Car();

        // when then
        NoAvailablePositionException noAvailablePositionException =
                Assertions.assertThrows(NoAvailablePositionException.class,
                        () -> superSmartParkingBoy.park(car));
        Assertions.assertEquals("No available position.", noAvailablePositionException.getMessage());
    }

    @Test
    void should_return_correct_parking_log_when_park_car_by_super_smart_parking_boy_given_car_with_correct_parking_lot() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(5);
        ParkingLot parkingLot3 = new ParkingLot(3);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2, parkingLot3));
        Car car = new Car();
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        parkingLot3.park(new Car());

        // when
        Ticket ticket = superSmartParkingBoy.park(car);

        // then
        Assertions.assertEquals(parkingLot2, ticket.getParkingLot());
    }

    @Test
    void should_return_correct_parking_log_when_park_car_by_super_smart_parking_boy_given_car_with_not_correct_parking_lot() {
        // given
        ParkingLot parkingLot1 = new ParkingLot(6);
        ParkingLot parkingLot2 = new ParkingLot(5);
        ParkingLot parkingLot3 = new ParkingLot(2);
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot1, parkingLot2, parkingLot3));
        parkingLot1.park(new Car());
        parkingLot2.park(new Car());
        parkingLot3.park(new Car());
        Car car = new Car();

        // when
        Ticket ticket = superSmartParkingBoy.park(car);

        // then
        Assertions.assertNotEquals(parkingLot2, ticket.getParkingLot());
    }
}
