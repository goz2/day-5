package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ParkingLotServiceManagerTest {
    @Test
    void should_return_ticket_when_park_car_given_parking_manager_a_car() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(2);
        ParkingLot parkingLot4 = new ParkingLot(2);
        ParkingLot parkingLot5 = new ParkingLot(2);
        ParkingLot parkingLot6 = new ParkingLot(2);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1, parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot3, parkingLot4));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot5, parkingLot6));

        parkingLotServiceManager.addParkingBoy(smartParkingBoy, smartParkingBoy, superSmartParkingBoy);

        Car car = new Car();

        // when
        Ticket ticket = parkingLotServiceManager.park(car);

        // then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_parking_manager_a_ticket() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(2);
        ParkingLot parkingLot4 = new ParkingLot(2);
        ParkingLot parkingLot5 = new ParkingLot(2);
        ParkingLot parkingLot6 = new ParkingLot(2);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1, parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot3, parkingLot4));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot5, parkingLot6));

        parkingLotServiceManager.addParkingBoy(smartParkingBoy, smartParkingBoy, superSmartParkingBoy);

        Car car = new Car();
        Ticket ticket = parkingLotServiceManager.park(car);

        // when
        Car car_ = parkingLotServiceManager.fetch(ticket);

        // then
        assertNotNull(car_);
    }

    @Test
    void should_return_two_car_when_fetch_car_given_parking_manager_two_ticket() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(2);
        ParkingLot parkingLot4 = new ParkingLot(2);
        ParkingLot parkingLot5 = new ParkingLot(2);
        ParkingLot parkingLot6 = new ParkingLot(2);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1, parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot3, parkingLot4));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot5, parkingLot6));

        parkingLotServiceManager.addParkingBoy(smartParkingBoy, smartParkingBoy, superSmartParkingBoy);

        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLotServiceManager.park(car1);
        Ticket ticket2 = parkingLotServiceManager.park(car2);

        // when
        Car car_1 = parkingLotServiceManager.fetch(ticket1);
        Car car_2 = parkingLotServiceManager.fetch(ticket2);

        // then
        assertNotNull(car_1);
        assertNotNull(car_2);
    }

    @Test
    void should_return_exception_when_fetch_car_given_parking_manager_wrong_ticket() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(2);
        ParkingLot parkingLot4 = new ParkingLot(2);
        ParkingLot parkingLot5 = new ParkingLot(2);
        ParkingLot parkingLot6 = new ParkingLot(2);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1, parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot3, parkingLot4));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot5, parkingLot6));

        parkingLotServiceManager.addParkingBoy(smartParkingBoy, smartParkingBoy, superSmartParkingBoy);
        Ticket ticket = new Ticket();

        // when then
        UnrecognizedException unrecognizedException = assertThrows(UnrecognizedException.class, () -> parkingLotServiceManager.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }

    @Test
    void should_return_exception_when_fetch_car_given_parking_manager_used_ticket() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(2);
        ParkingLot parkingLot4 = new ParkingLot(2);
        ParkingLot parkingLot5 = new ParkingLot(2);
        ParkingLot parkingLot6 = new ParkingLot(2);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1, parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot3, parkingLot4));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot5, parkingLot6));

        parkingLotServiceManager.addParkingBoy(smartParkingBoy, smartParkingBoy, superSmartParkingBoy);
        Car car = new Car();
        Ticket ticket = parkingLotServiceManager.park(car);
        parkingLotServiceManager.fetch(ticket);

        // when then
        UnrecognizedException unrecognizedException = assertThrows(UnrecognizedException.class, () -> parkingLotServiceManager.fetch(ticket));
        assertEquals("Unrecognized parking ticket", unrecognizedException.getMessage());
    }


    @Test
    void should_return_exception_when_park_car_given_parking_lot_no_position() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        ParkingLot parkingLot3 = new ParkingLot(0);
        ParkingLot parkingLot4 = new ParkingLot(0);
        ParkingLot parkingLot5 = new ParkingLot(0);
        ParkingLot parkingLot6 = new ParkingLot(0);

        StandardParkingBoy standardParkingBoy = new StandardParkingBoy();
        standardParkingBoy.addParkingLot(parkingLot1, parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(List.of(parkingLot3, parkingLot4));
        SuperSmartParkingBoy superSmartParkingBoy = new SuperSmartParkingBoy(List.of(parkingLot5, parkingLot6));

        parkingLotServiceManager.addParkingBoy(smartParkingBoy, smartParkingBoy, superSmartParkingBoy);
        Car car = new Car();

        // when then
        NoParkingBoyAvailableException noParkingBoyAvailableException = assertThrows(NoParkingBoyAvailableException.class, () -> parkingLotServiceManager.park(car));
        assertEquals("No parking boy available.", noParkingBoyAvailableException.getMessage());
    }

    @Test
    void should_return_ticket_when_park_car_given_parking_manager_own_parking_loy_a_car() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingLot(parkingLot1, parkingLot2);

        // when
        Ticket ticket = parkingLotServiceManager.parkToOwnParkingLot(car);

        // then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_car_given_parking_manager_own_parking_loy_a_ticket() {
        // given
        Car car = new Car();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        parkingLotServiceManager.addParkingLot(parkingLot1, parkingLot2);
        Ticket ticket = parkingLotServiceManager.parkToOwnParkingLot(car);

        // when
        Car car_ = parkingLotServiceManager.fetchFromOwnParkingLot(ticket);

        // then
        Assertions.assertNotNull(car_);
    }

}