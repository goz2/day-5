package com.parkinglot.strategy;

import com.parkinglot.*;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ParkingLotServiceManagerTest {
    @Test
    void should_return_ticket_when_park_car_given_parking_manager_a_car() {
        // given
        ParkingLotServiceManager parkingLotServiceManager = new ParkingLotServiceManager();
        ParkingLot parkingLot1 = new ParkingLot(2);
        ParkingLot parkingLot2 = new ParkingLot(2);
        ParkingLot parkingLot3 = new ParkingLot(2);
        ParkingLot parkingLot4 = new ParkingLot(5);
        ParkingLot parkingLot5 = new ParkingLot(4);
        ParkingLot parkingLot6 = new ParkingLot(2);
        ParkingLot parkingLot7 = new ParkingLot(6);
        ParkingBoy standardParkingBoy = new ParkingBoy(List.of(parkingLot1, parkingLot2));
        standardParkingBoy.setParkingStrategy(new ParkingStrategyFactory("com.parkinglot.strategy.SequentialParkingStrategy"));

        ParkingBoy smartParkingBoy = new ParkingBoy(List.of(parkingLot3, parkingLot4));
        smartParkingBoy.setParkingStrategy(new ParkingStrategyFactory("com.parkinglot.strategy.MostEmptyPositionsParkingStrategy"));

        ParkingBoy superSmartParkingBoy = new ParkingBoy(List.of(parkingLot5, parkingLot6, parkingLot7));
        superSmartParkingBoy.setParkingStrategy(new ParkingStrategyFactory("com.parkinglot.strategy.HighestAvailablePositionRateParkingStrategy"));

        parkingLotServiceManager.addParkingBoy(standardParkingBoy, smartParkingBoy, superSmartParkingBoy);

        // when
        Ticket ticket1 = parkingLotServiceManager.park(new Car());

        // then
        assertEquals(parkingLot1, ticket1.getParkingLot());

        parkingLot1.setCapacity(0);
        parkingLot2.setCapacity(0);
        Ticket ticket2 = parkingLotServiceManager.park(new Car());
        assertEquals(parkingLot4, ticket2.getParkingLot());

        parkingLot3.setCapacity(0);
        parkingLot4.setCapacity(0);
        parkingLotServiceManager.park(new Car());
        parkingLotServiceManager.park(new Car());
        parkingLotServiceManager.park(new Car());
        Ticket ticket3 = parkingLotServiceManager.park(new Car());
        assertEquals(parkingLot7, ticket3.getParkingLot());
    }
}