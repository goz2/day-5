## ORID homework


	O (Objective): What did we learn today? What activities did you do? What scenes have impressed you?
	R (Reflective): Please use one word to express your feelings about today's class.
	I (Interpretive): What do you think about this? What was the most meaningful aspect of this activity?D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?
	D (Decisional): Where do you most want to apply what you have learned today? What changes will you make?


- O (Objective): Today's main exercise was Test-Driven Development (TDD). We learned about the principles and practices of TDD and engaged in hands-on activities to apply these concepts. The scenes that impressed me were witnessing the incremental development process and seeing the immediate feedback provided by the test cases.

- R (Reflective): Enthusiastic.

- I (Interpretive): TDD is an effective approach to software development that emphasizes writing tests before implementing the actual code. It promotes a systematic and disciplined way of building software by focusing on testability, modular design, and ensuring code correctness.

- D (Decisional): I am most eager to apply what I have learned today in my future programming projects. I want to incorporate TDD into my development workflow to ensure that I write comprehensive tests for my code and achieve better code quality. 
